BUILDDIR = build
SRCDIR = src

CC = gcc
CFLAGS = -O0 -g


all: $(BUILDDIR)/tracee $(BUILDDIR)/tracer

$(BUILDDIR):
	mkdir -p $@

$(BUILDDIR)/tracee: $(SRCDIR)/main-tracee.c | $(BUILDDIR)
	$(CC) $(CFLAGS) -o $@ $<

$(BUILDDIR)/tracer: $(SRCDIR)/main-tracer.c | $(BUILDDIR)
	$(CC) $(CFLAGS) -o $@ $<

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
