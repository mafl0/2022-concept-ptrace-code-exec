#!/usr/bin/env bash

BUILDDIR=build
TRACEE=$BUILDDIR/tracee
TRACER=$BUILDDIR/tracer

TRACEE_PID=0

function run_if_exists() {
  local PROGRAM=$1
  shift

  if [ -f $PROGRAM ]; then
    $PROGRAM $@
  else
    echo "[shell]  file $PROGRAM does not exist"
  fi
}

function main() {
  echo "[shell]  running make, just to be sure"
  make -j8 -l8 -B &> /dev/null

  echo "[shell]  killing all old intances of $TRACEE"
  pkill tracee &> /dev/null
  
  echo "[shell]  running tracee"
  run_if_exists $TRACEE &
  TRACEE_PID=$(pgrep tracee)
  echo "[shell]  tracee pid = $TRACEE_PID"

  echo "[shell]  running tracer"
  run_if_exists $TRACER -p $TRACEE_PID
}

main $@
