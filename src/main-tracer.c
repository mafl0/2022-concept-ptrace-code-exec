#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/wait.h>

typedef struct Options {
  pid_t pid;
  unsigned long baseAddress; // found automatically by baseAddress() using /proc/pid/maps
} OptionsT;

OptionsT * newOptions() {
  OptionsT * o = (OptionsT *) calloc(sizeof(OptionsT), 1);

  return o;
}

// parse options
void parseOptions(int argc, char ** argv, OptionsT * options) {

  char option;

  // Parse command line options, p required
  while ((option = getopt(argc, argv, "p:")) != -1) {
    switch(option) {
      case 'p':
        options->pid = (pid_t) atoi(optarg);
        break;
      default:
        // not supported
        break;
    }
  }
}

// find program base
unsigned long baseAddress(OptionsT * options) {
  unsigned long ret = 0;
  char buf[256];

  snprintf(buf, 256, "/proc/%u/maps", options->pid);
  FILE * map_file = fopen(buf, "r");

  for (char c = fgetc(map_file); c != '-'; c = fgetc(map_file)) {
    ret <<= 4;                // on to the next nibble
    if ('0' <= c && c <= '9') {
      ret += c - '0';         // decdigit
    } else {
      ret += (c - 'a') + 0xa; // hexdigit, lowercase
    }
  } 

  fclose(map_file);

  printf("[tracer] Program base of process %d is at %p\n", options->pid, (void *) ret);

  return ret;
}

// attach
void attach(OptionsT * options) {
  
  if (ptrace(PTRACE_ATTACH, options->pid, 0L, 0L) == -1L) {
    printf("[tracer] failed to attach to %d, %s\n", options->pid, strerror(errno));
    
    abort();
  }

  /* wait for the SIGSTOP to take place. */
  if (waitpid(options->pid, NULL, 0L) == -1) {
    printf("[tracer] there was an error waiting for the target to stop: %s\n", strerror(errno));
  }

  printf("[tracer] Successfully attached to process %d\n", options->pid);
}

int main(int argc, char ** argv) {

  // option parsing
  OptionsT * options = newOptions();
  parseOptions(argc, argv, options);

  // attach to the program and get the base address
  attach(options);
  options->baseAddress = baseAddress(options);

  /**
   * To call a function in the remote process we need to
   *
   *  0. Know the offset from the base where the remote function is located
   *  1. Get the current context and save it
   *  2. Replace the context with one that is about to call mmap
   *  3. Replace the code at the instruction pointer
   *  4. Run custom code
   *  5. Restore the original code at the instruction pointer, unmap the mmap'ed
   *     memory
   *  6. Restore the original context
   */

  /**
   * 0. Know the offset from the base where the remote function is located
   *
   * We can get this by static analysis on decompilation, debug symbols if they
   * are present. w/e
   */
  const unsigned long remoteFunctionOffset = 0x1169UL;

  // 1. get the current context and save it
  struct user_regs_struct oldContext;
  ptrace(PTRACE_GETREGS, options->pid, NULL, &oldContext);

  /**
   * 2. replace the context with one that is about to call mmap
   *
   *  We want to call
   *    mmap(void * addr, size_t len, int prot, int flags, int fildes, off_t off);
   *
   *  with                                    because we want...
   *    addr   = 0                            to ignore this argument, any address is fine
   *    len    = PAGE_SIZE                    only one page in bytes
   *    prot   = PROT_READ | PROT_EXEC        to read and execute what we write in the page
   *    flags  = MAP_PRIVATE | MAP_ANONYMOUS  to not change any underlying object with MAP_PRIVATE and to zero the resulting page with MAP_ANONYMOUS
   *    fildes = -1                           ?
   *    off    = 0                            we want to start writing at the beginning of the page
   *
   * then we should set:
   *
   * mmap corresponds to syscall #9 according to asm/unistd_64.h (follow man 2 syscall)
   * rax = 9
   *
   * according to the SYSV calling convention:
   * rdi, rsi, rdx, rcx, r8, r9 integer args 1..6
   *
   * rdi = 0
   * rsi = PAGE_SIZE
   * rdx = PROT_READ | PROT_EXEC
   * rcx = MAP_PRIVATE | MAP_ANONYMOUS     (this was r10 in the example (static chain pointer?))
   * r8  = -1
   * r9  = 0
   *
   * all the other registers should remain equal, which we do with memcpy
   *
   */
  struct user_regs_struct newContext;
  memcpy(&newContext, &oldContext, sizeof(newContext));

  newContext.rax = 9;

  newContext.rdi = 0;
  newContext.rsi = PAGE_SIZE;
  newContext.rdx = PROT_READ | PROT_EXEC;
  newContext.rcx = MAP_PRIVATE | MAP_ANONYMOUS;
  newContext.r8  = -1;
  newContext.r9  = 0;

  ptrace(PTRACE_SETREGS, options->pid, 0L, &newContext);
  
  /**
   * 3. replace the code at the instruction pointer
   *
   * We don't know where the code was paused, and we don't want to disturb the 
   * execution after we're done with our injection. So what we want is to
   * replace whatever is at the instruction pointer with:
   *
   * syscall ; calls the mmap that we just prepared
   * jmp rax ; jumps into the code that we inject, which resides in the page
   *           that mmap gave us area that we requested.
   *
   * this assembles to
   *
   * 0f 05 (syscall) ff (jmp) e0 (rax)
   *
   * so we need 4 bytes
   *
   */
  unsigned char oldCode[4] = {0x00, 0x00, 0x00, 0x00}
              , newCode[4] = {0x0f, 0x05, 0xff, 0xe0}
              ;
  
  unsigned long remoteMmapPage = 0UL;

  unsigned char * addr = NULL;
  for (size_t i = 0; i < 4; ++i) {
    // force byte-sized address increment
    addr = (unsigned char *) oldContext.rip + i; 

    // Store the original, replace with new
    oldCode[i] = (unsigned char) ptrace(PTRACE_PEEKTEXT, options->pid, addr, 0L);
    ptrace(PTRACE_POKETEXT, options->pid, addr, &newCode);
  }

  // single step `syscall', executes mmap
  ptrace(PTRACE_SINGLESTEP, options->pid, 0L, 0L);

  // Get the return value from the mmap call, which is the address where the
  // page resides on the remote side. Re-use newContext.
  ptrace(PTRACE_GETREGS, options->pid, 0L, newContext);
  remoteMmapPage = newContext.rax;

  /**
   * 4. Write custom injected code to mmap'ed area.
   *
   * In our case we want to call void execme(int x) at @remoteFunctionOffset@
   *
   * so we need to prepare a SYSV ABI function call with 1 integer argument:
   *
   * rdi = first argument
   *
   * mov X, %rdi ; where X is your argument, 1 in this example
   * call execme
   *
   * bf 00 00 00 01          mov    $0x00000001,%edi
   * e8 b1 ff ff ff          call   1169 <execme>
   * 
   * Above code stolen from decompilation of the original. Saves us from really
   * having to understand x86
   */

  //                              change these to change argument
  //                                     v     v     v     v
  unsigned char injectedCode[] = { 0xbf, 0x00, 0x00, 0x00, 0x01
                                 , 0xe8, 0xb1, 0xff, 0xff, 0xff
                                 };

  for (size_t i = 0; i < sizeof(injectedCode) / sizeof(*injectedCode); ++i) {
    addr = (unsigned char *) remoteMmapPage + i;
    ptrace(PTRACE_POKETEXT, options->pid, addr, injectedCode[i]);
  }

  // clean up
  free(options);

  return EXIT_SUCCESS;
}
