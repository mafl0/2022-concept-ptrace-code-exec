#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

void execme(int arg) {
  printf("[tracee] \tvoid execme(int arg) called succesfully with arg=%d\n", arg);
}

int main(int argc, char ** argv) {

  printf("[tracee] Executing once from tracee for testing purposes:\n");
  execme(-1);

  printf("[tracee] Waiting for debugger to trigger execme() externally...\n");

  fflush(stdout);

  for (;;) {
    sleep(UINT32_MAX); // just idle "forever".
  }


  return EXIT_SUCCESS;
}
